TP4 : Real services


Partie 1 : Partitionnement du serveur de stockage

🌞 Partitionner le disque à l'aide de LVM


créer 2 physical volumes (PV) : 
```
[lunacf@storage ~]$ lsblk

NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
sda           8:0    0  8.2G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0  7.2G  0 part
  ├─rl-root 253:0    0  6.4G  0 lvm  /
  └─rl-swap 253:1    0  840M  0 lvm  [SWAP]
sdb           8:16   0    2G  0 disk
sdc           8:32   0    2G  0 disk
sr0          11:0    1 1024M  0 rom



[lunacf@storage ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.

[lunacf@storage ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.



[lunacf@storage ~]$ sudo pvs

  PV         VG Fmt  Attr PSize PFree
  /dev/sdb      lvm2 ---  2.00g 2.00g
  /dev/sdc      lvm2 ---  2.00g 2.00g
```


créer un nouveau volume group (VG):
```
[lunacf@storage ~]$ sudo vgcreate storage /dev/sdb
  Volume group "storage" successfully created

[lunacf@storage ~]$ sudo vgextend storage /dev/sdc
  Volume group "storage" successfully extended



[lunacf@storage ~]$ sudo vgs

  VG      #PV #LV #SN Attr   VSize VFree
  storage   2   0   0 wz--n- 3.99g 3.99g
```


créer un nouveau logical volume (LV) : ce sera la partition utilisable
```
[lunacf@storage ~]$ sudo lvcreate -l 100%FREE storage -n last_storage

  Logical volume "last_storage" created.

[lunacf@storage ~]$ sudo lvs

  LV           VG      Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  last_storage storage -wi-a----- 3.99g
```


🌞 Formater la partition

```
[lunacf@storage ~]$ sudo lvdisplay

  Devices file sys_wwid t10.ATA_VBOX_HARDDISK_VBfac0259e-90096643 PVID vq2Urt3zRWdrLSc53UFk9K5RDordGRVG last seen on /dev/sda2 not found.
  --- Logical volume ---
  LV Path                /dev/storage/last_storage
  LV Name                last_storage
  VG Name                storage
  LV UUID                LtWGUy-6cTv-X9eS-TS4B-tWa2-q98M-qMuNon
  LV Write Access        read/write
  LV Creation host, time storage, 2024-02-19 16:47:54 +0100
  LV Status              available
  # open                 0
  LV Size                3.99 GiB
  Current LE             1022
  Segments               2
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:2
```


🌞 Monter la partition

```
[lunacf@storage ~]$ df -h |grep storage
/dev/mapper/storage-last_storage  3.9G   24K  3.7G   1% /mnt/storage1


[lunacf@storage ~]$ ls -ld /mnt/storage1
drwxr-xr-x. 2 root root 6 Feb 19 16:58 /mnt/storage1


[lunacf@storage ~]$ sudo blkid | grep storage
/dev/mapper/storage-last_storage: UUID="43d1d5b8-6c65-425e-818a-ddebb8f1a866" TYPE="ext4"


[lunacf@storage ~]$ sudo cat /etc/fstab | grep storage
UUID=43d1d5b8-6c65-425e-818a-ddebb8f1a866   /mnt/storage1   ext4   defaults   0  0


[lunacf@storage ~]$ mount | grep /mnt/storage1
/dev/mapper/storage-last_storage on /mnt/storage1 type ext4 (rw,relatime,seclabel)


[lunacf@storage ~]$ ls /mnt/storage1
lost+found
```


Partie 2 : Serveur de partage de fichiers

🌞 Donnez les commandes réalisées sur le serveur NFS `storage.tp4.linux`


commandes
```
[lunacf@storage storage1]$ sudo mkdir /mnt/storage1/site_web_2


[lunacf@storage storage1]$ ls
lost+found  site_web_1  site_web_2


[lunacf@storage storage1]$ ls -dl /mnt/storage1/site_web_1
drwxr-xr-x. 2 root root 4096 Feb 20 14:45 /mnt/storage1/site_web_1


[lunacf@storage storage1]$ ls -dl /mnt/storage1/site_web_2
drwxr-xr-x. 2 root root 4096 Feb 20 14:47 /mnt/storage1/site_web_2


[lunacf@storage storage1]$ sudo chown nobody /mnt/storage1//site_web_1

[lunacf@storage storage1]$ sudo chown nobody /mnt/storage1//site_web_2



[lunacf@storage storage1]$ sudo systemctl status nfs-server

● nfs-server.service - NFS server and services
     Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; preset: disabled)
    Drop-In: /run/systemd/generator/nfs-server.service.d
             └─order-with-mounts.conf
     Active: active (exited) since Tue 2024-02-20 15:10:48 CET; 3s ago
    Process: 12302 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
    Process: 12303 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
    Process: 12313 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=e>
   Main PID: 12313 (code=exited, status=0/SUCCESS)
        CPU: 77ms

Feb 20 15:10:48 storage systemd[1]: Starting NFS server and services...
Feb 20 15:10:48 storage systemd[1]: Finished NFS server and services.


[lunacf@storage /]$ sudo firewall-cmd --permanent --list-all | grep services
  services: cockpit dhcpv6-client ssh
```


/etc/exports
```
[lunacf@storage storage1]$ sudo cat /etc/exports

/storage1/site_web_1/ 10.0.4.11(rw,sync)
/storage1/site_web_2/ 10.0.4.11(rw,sync)

```



🌞 Donnez les commandes réalisées sur le client NFS `web.tp4.linux`

```
[lunacf@web var]$ sudo mkdir -p /var/www/site_web_1/
[lunacf@web var]$ sudo mkdir -p /var/www/site_web_2/

[lunacf@web var]$ cd www
[lunacf@web www]$ ls
site_web_1  site_web_2



[lunacf@web www]$ sudo mount 10.0.4.10:/mnt/storage1//site_web_1 /var/www/site_web_1
[lunacf@web www]$ sudo mount 10.0.4.10:/mnt/storage1//site_web_2 /var/www/site_web_2

[lunacf@web www]$ df -h

Filesystem                          Size  Used Avail Use% Mounted on
devtmpfs                            4.0M     0  4.0M   0% /dev
tmpfs                               386M     0  386M   0% /dev/shm
tmpfs                               155M  3.8M  151M   3% /run
/dev/mapper/rl-root                 6.4G  1.2G  5.2G  19% /
/dev/sda1                          1014M  220M  795M  22% /boot
tmpfs                                78M     0   78M   0% /run/user/1000
10.0.4.10:/mnt/storage1/site_web_1  3.9G     0  3.7G   0% /var/www/site_web_1
10.0.4.10:/mnt/storage1/site_web_2  3.9G     0  3.7G   0% /var/www/site_web_2


[lunacf@web www]$ sudo touch /var/www/site_web_1/site1.test

[lunacf@web www]$ ls -l /var/www/site_web_1/site1.test
-rw-r--r--. 1 nobody nobody 0 Feb 20 16:11 /var/www/site_web_1/site1.test

[lunacf@web www]$ ls -l /var/www/site_web_2/site2.test
-rw-r--r--. 1 nobody nobody 0 Feb 20 16:13 /var/www/site_web_2/site2.test
```


```
[lunacf@web www]$ sudo cat /etc/fstab | grep storage

10.0.4.10:/mnt/storage1/site_web_1    /var/www/site_web_1      nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
10.0.4.10:/mnt/storage1/site_web_2    /var/www/site_web_2      nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
```



Partie 3 : Serveur web

2. Install


🌞 Installez NGINX

```
[lunacf@web www]$ sudo dnf install nginx

Last metadata expiration check: 2:16:04 ago on Tue 20 Feb 2024 02:20:26 PM CET.
Dependencies resolved.

Installed:
  nginx-1:1.20.1-14.el9_2.1.x86_64                              nginx-core-1:1.20.1-14.el9_2.1.x86_64
  nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                   rocky-logos-httpd-90.14-2.el9.noarch

Complete!
```


3. Analyse


🌞 Analysez le service NGINX

```
[lunacf@web www]$ sudo systemctl start nginx

[lunacf@web www]$ sudo systemctl status nginx

● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; preset: disabled)
     Active: active (running) since Tue 2024-02-20 16:42:44 CET; 10s ago
    Process: 12222 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 12223 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 12224 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 12225 (nginx)
      Tasks: 2 (limit: 4673)
     Memory: 1.9M
        CPU: 64ms
     CGroup: /system.slice/nginx.service
             ├─12225 "nginx: master process /usr/sbin/nginx"
             └─12226 "nginx: worker process"

Feb 20 16:42:44 web systemd[1]: Starting The nginx HTTP and reverse proxy server...
Feb 20 16:42:44 web nginx[12223]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Feb 20 16:42:44 web nginx[12223]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Feb 20 16:42:44 web systemd[1]: Started The nginx HTTP and reverse proxy server.
```

4. Visite du service web


🌞 Configurez le firewall pour autoriser le trafic vers le service NGINX

```

```

🌞 Accéder au site web

```

```

🌞 Vérifier les logs d'accès

```

```


5. Modif de la conf du serveur web


🌞 Changer le port d'écoute

```

```

🌞 Changer l'utilisateur qui lance le service

```

```

🌞 Changer l'emplacement de la racine Web

```

```


6. Deux sites web sur un seul serveur


🌞 Repérez dans le fichier de conf

```

```

🌞 Créez le fichier de configuration pour le premier site

```

```

🌞 Créez le fichier de configuration pour le deuxième site

```

```

🌞 Prouvez que les deux sites sont disponibles

```

```