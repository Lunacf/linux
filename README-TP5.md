TP 5 : We do a little scripting


I. Script carte d'identité

Rendu


🌞 Vous fournirez dans le compte-rendu Markdown, en plus du fichier, un exemple d'exécution avec une sortie
```
[lunacf@TP5 idcard]$ ./idcard.sh
Machine name : TP5
OS Rocky Linux 9.2 (Blue Onyx) and kernel version is Linux 
IP : 10.5.1.10/24 
RAM : 376 Mo memory available on 809 Mo total memory
Disk : 5.3G space left
Top 5 processes by RAM usage : 
 - node
 - node
 - node
 - python3
 - NetworkManager
Listening ports : 
 - 323 udp : chronyd 
 - 46079 tcp : session-3 
 - 22 tcp : sshd 
PATH directories :
/home/lunacf/.vscode-server/bin/019f4d1419fbc8219a181fab7892ebccf7ee29a2/bin/remote-cli
/home/lunacf/.local/bin
/home/lunacf/bin
/usr/local/bin
/usr/bin
/usr/local/sbin
/usr/sbin
Here is your random cat (jpg file) : https://cdn2.thecatapi.com/images/aia.jpg
```

```
PS C:\Users\Utilisateur> scp lunacf@10.5.1.10:/srv/idcard/idcard.sh  .\Documents\YNOV\linux\linux
```


II. Script youtube-dl

1. Premier script youtube-dl
