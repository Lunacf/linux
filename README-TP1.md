TP1 : Casser avant de construire

2. Fichier

🌞 Supprimer des fichiers

```
suppression de : 
    vmlinuz-0-rescue-0e0298aa608f4081946e51b757db2728
    vmlinuz-5.14.0-284.11.1.el9_2.x86_64


error: ../../grub-core/fs/fshelp.c:257:file
'/vmlinuz-5.14.0-284.11.1.el9_2.x86_64' not found.
error: ../../grub-core/loader/i386/pc/linux.c:422:you need to load the kernel first.
```


3. Utilisateurs

🌞 Mots de passe

```
[root@crash ~]# cat /etc/passwd | grep /bin
    root:x:0:0:root:/root:/bin/bash
    bin:x:1:1:bin:/bin:/sbin/nologin
    sync:x:5:0:sync:/sbin:/bin/sync
    lunacf:x:1000:1000:lunacf:/home/lunacf:/bin/bash
```


🌞 Another way ?
```
[root@crash ~]# rm /etc/passwd

on peut se connecter à root mais pas à lunacf.



[root@crash ~]# rm /etc/shadow

on ne peut se connecter à aucun utilisateur.
```


4. Disques

🌞 Effacer le contenu du disque dur

```
[root@crash ~]# dd if=/dev/zero of=/dev/sdA bs=8M
dd: error writing '/dev/sdA': No space left on device
1+0 records in
0+0 records out
4194304 bytes (4.2 MB, 4.0 MiB) copied, 0.010929 s, 384 MB/s
```

5. Malware

🌞 Reboot automatique

```

```

6. You own way

🌞 Trouvez 4 autres façons de détuire la machine

```

```

7. Bonus : remédiations

✨ Trouver des remédiations

```

```