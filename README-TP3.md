TP3 : Services

I. Service SSH

1. Analyse du service

🌞 S'assurer que le service `sshd` est démarré

```
[lunacf@node1 ~]$ sudo systemctl status | grep sshd
           │ ├─sshd.service
           │ │ └─676 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"
               │ ├─1384 "sshd: lunacf [priv]"
               │ ├─1388 "sshd: lunacf@pts/0"
```


🌞 Analyser les processus liés au service SSH

```
[lunacf@node1 ~]$ ps

    PID TTY          TIME CMD
   1389 pts/0    00:00:00 bash
   1447 pts/0    00:00:00 ps


[lunacf@node1 ~]$ systemctl status sshd

● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; preset: enabled)
     Active: active (running) since Mon 2024-02-19 19:51:29 CET; 14min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 676 (sshd)
      Tasks: 1 (limit: 4673)
     Memory: 4.8M
        CPU: 223ms
     CGroup: /system.slice/sshd.service
             └─676 "sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups"

Feb 19 19:51:29 node1.tp3.b1 systemd[1]: Starting OpenSSH server daemon...
Feb 19 19:51:29 node1.tp3.b1 sshd[676]: main: sshd: ssh-rsa algorithm is disabled
Feb 19 19:51:29 node1.tp3.b1 sshd[676]: Server listening on 0.0.0.0 port 22.
Feb 19 19:51:29 node1.tp3.b1 sshd[676]: Server listening on :: port 22.
Feb 19 19:51:29 node1.tp3.b1 systemd[1]: Started OpenSSH server daemon.
Feb 19 19:56:40 node1.tp3.b1 sshd[1384]: main: sshd: ssh-rsa algorithm is disabled
Feb 19 19:56:43 node1.tp3.b1 sshd[1384]: Accepted password for lunacf from 10.3.1.1 port 56915 ssh2
Feb 19 19:56:43 node1.tp3.b1 sshd[1384]: pam_unix(sshd:session): session opened for user lunacf(uid=1000) by (uid=0)
```


🌞 Déterminer le port sur lequel écoute le service SSH

```
[lunacf@node1 ~]$ ss | grep ssh
tcp   ESTAB 0      52                        10.3.1.11:ssh       10.3.1.1:56915
```


🌞 Consulter les logs du service SSH

```
[lunacf@node1 ~]$ journalctl | grep ssh

Feb 19 19:51:26 node1.tp3.b1 systemd[1]: Created slice Slice /system/sshd-keygen.
Feb 19 19:51:28 node1.tp3.b1 systemd[1]: Reached target sshd-keygen.target.
Feb 19 19:51:29 node1.tp3.b1 sshd[676]: main: sshd: ssh-rsa algorithm is disabled
Feb 19 19:51:29 node1.tp3.b1 sshd[676]: Server listening on 0.0.0.0 port 22.
Feb 19 19:51:29 node1.tp3.b1 sshd[676]: Server listening on :: port 22.
Feb 19 19:56:40 node1.tp3.b1 sshd[1384]: main: sshd: ssh-rsa algorithm is disabled
Feb 19 19:56:43 node1.tp3.b1 sshd[1384]: Accepted password for lunacf from 10.3.1.1 port 56915 ssh2
Feb 19 19:56:43 node1.tp3.b1 sshd[1384]: pam_unix(sshd:session): session opened for user lunacf(uid=1000) by (uid=0)



[lunacf@node1 log]$ sudo cat secure | tail

Feb 19 20:02:56 node1 sudo[1439]: pam_unix(sudo:session): session closed for user root
Feb 19 20:16:22 node1 sudo[1469]:  lunacf : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat lastlog
Feb 19 20:16:22 node1 sudo[1469]: pam_unix(sudo:session): session opened for user root(uid=0) by lunacf(uid=1000)
Feb 19 20:16:22 node1 sudo[1469]: pam_unix(sudo:session): session closed for user root
Feb 19 20:19:05 node1 sudo[1476]:  lunacf : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat dnf.log
Feb 19 20:19:05 node1 sudo[1476]: pam_unix(sudo:session): session opened for user root(uid=0) by lunacf(uid=1000)
Feb 19 20:19:05 node1 sudo[1476]: pam_unix(sudo:session): session closed for user root
Feb 19 20:20:45 node1 sudo[1481]:  lunacf : TTY=pts/0 ; PWD=/var/log ; USER=root ; COMMAND=/bin/cat secure
Feb 19 20:20:45 node1 sudo[1481]: pam_unix(sudo:session): session opened for user root(uid=0) by lunacf(uid=1000)
Feb 19 20:20:45 node1 sudo[1481]: pam_unix(sudo:session): session closed for user root
```

2. Modification du service

🌞 Identifier le fichier de configuration du serveur SSH

```
[lunacf@node1 ]$ sudo cat /etc/shh/sshd_config
```

🌞 Modifier le fichier de conf


demander à votre shell de vous fournir un nombre aléatoire
```
[lunacf@node1 ssh]$ echo $RANDOM
12968
```

-changez le port d'écoute du serveur SSH

```
[lunacf@node1 ssh]$ sudo cat sshd_config | grep Port
Port 12968
GatewayPorts yes


-gérer le firewall
```
[lunacf@node1 ssh]$ sudo firewall-cmd --remove-port=22/tcp --permanent
success
[lunacf@node1 ssh]$ sudo firewall-cmd --add-port=12968/tcp --permanent
success

[lunacf@node1 ~]$ sudo firewall-cmd --list-all | grep ports
  ports: 12968/tcp
  forward-ports:
  source-ports:
```

🌞 Redémarrer le service

```
[lunacf@node1 ~]$ sudo systemctl restart sshd
```

🌞 Effectuer une connexion SSH sur le nouveau port

```
PS C:\Users\Utilisateur> ssh lunacf@10.3.1.11 -p 12968
lunacf@10.3.1.11's password:
Last login: Mon Mar 11 01:00:07 2024 from 10.3.1.1
```



II. Service HTTP

1. Mise en place


🌞 Installer le serveur NGINX

```
[lunacf@node1 ~]$ sudo dnf search nginx
============================================= Name Exactly Matched: nginx ==============================================nginx.x86_64 : A high performance web server and reverse proxy server
============================================ Name & Summary Matched: nginx =============================================
nginx-all-modules.noarch : A meta package that installs all available Nginx modules
nginx-core.x86_64 : nginx minimal core
nginx-filesystem.noarch : The basic directory layout for the Nginx server
nginx-mod-http-image-filter.x86_64 : Nginx HTTP image filter module
nginx-mod-http-perl.x86_64 : Nginx HTTP perl module
nginx-mod-http-xslt-filter.x86_64 : Nginx XSLT module
nginx-mod-mail.x86_64 : Nginx mail modules
nginx-mod-stream.x86_64 : Nginx stream modules
pcp-pmda-nginx.x86_64 : Performance Co-Pilot (PCP) metrics for the Nginx Webserver


[lunacf@node1 ~]$ sudo dnf install nginx

Installing:
 nginx                     x86_64         1:1.20.1-14.el9_2.1           appstream          36 k

Installed:
  nginx-1:1.20.1-14.el9_2.1.x86_64                              nginx-core-1:1.20.1-14.el9_2.1.x86_64
  nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                   rocky-logos-httpd-90.15-2.el9.noarch

Complete!
```

🌞 Démarrer le service NGINX

```
[lunacf@node1 ~]$ sudo systemctl start nginx
[lunacf@node1 ~]$ sudo systemctl enable nginx
[lunacf@node1 ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; preset: disabled)
     Active: active (running) since Mon 2024-03-11 01:45:48 CET; 59s ago
   Main PID: 11373 (nginx)
      Tasks: 2 (limit: 4673)
     Memory: 1.9M
        CPU: 66ms
     CGroup: /system.slice/nginx.service
             ├─11373 "nginx: master process /usr/sbin/nginx"
             └─11374 "nginx: worker process"
```

🌞 Déterminer sur quel port tourne NGINX

```
[lunacf@node1 ~]$ sudo ss -tulnp | grep nginx
tcp   LISTEN 0      511          0.0.0.0:80         0.0.0.0:*    users:(("nginx",pid=11374,fd=6),("nginx",pid=11373,fd=6))

[lunacf@node1 ~]$ sudo firewall-cmd --list-all | grep ports
  ports: 12968/tcp 80/tcp
  forward-ports:
  source-ports:
```

🌞 Déterminer les processus liés au service NGINX

```
[lunacf@node1 ~]$ ps aux | grep nginx
root       11592  0.0  0.1  10108   952 ?        Ss   02:12   0:00 nginx: master process /usr/sbin/nginx
nginx      11593  0.0  0.6  13908  4824 ?        S    02:12   0:00 nginx: worker process
lunacf     11601  0.0  0.2   6408  2184 pts/0    S+   02:20   0:00 grep --color=auto nginx
```

🌞 Déterminer le nom de l'utilisateur qui lance NGINX

```
[lunacf@node1 ~]$ sudo cat /etc/passwd | grep nginx
nginx:x:991:991:Nginx web server:/var/lib/nginx:/sbin/nologin
```

🌞 Test !

```
Luna@LAPTOP-2R2NMLK2 MINGW64 ~
$ curl http://10.3.1.11:80 | head -n 7
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0  2624k      0 --:--:-- --:--:-- --:--:-- 3720k
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">

```


2. Analyser la conf de NGINX


🌞 Déterminer le path du fichier de configuration de NGINX

```
[lunacf@node1 ~]$ ls -al /etc/nginx/nginx.conf
-rw-r--r--. 1 root root 2334 Oct 16 20:00 /etc/nginx/nginx.conf
```

🌞 Trouver dans le fichier de conf

```
[lunacf@node1 ~]$ sudo cat /etc/nginx/nginx.conf | grep server -A 10
    server {
        listen       80;
        listen       [::]:80;
        server_name  _;
        root         /usr/share/nginx/html;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```
```
[lunacf@node1 ~]$ sudo cat /etc/nginx/nginx.conf | grep ^include
include /usr/share/nginx/modules/*.conf;
```

3. Déployer un nouveau site web


🌞 Créer un site web

```

```

🌞 Gérer les permissions

```

```

🌞 Adapter la conf NGINX

```

```

🌞 Visitez votre super site web

```

```