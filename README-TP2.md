```
Partie 1 : Files and users
```


I. Fichiers

1. Find me


🌞 Trouver le chemin vers le répertoire personnel de votre utilisateur
```
[lunacf@tp2 ~]$ pwd

/home/lunacf
```

🌞 Trouver le chemin du fichier de logs SSH
```
[lunacf@tp2 ~]$ sudo cat /var/log/secure |grep ssh

Jan 22 16:33:28 tp2 sshd[682]: Server listening on 0.0.0.0 port 22.
Jan 22 16:33:28 tp2 sshd[682]: Server listening on :: port 22.
Jan 22 15:59:40 tp2 sshd[1519]: Accepted password for lunacf from 10.0.2.1 port 65495 ssh2
Jan 22 15:59:40 tp2 sshd[1519]: pam_unix(sshd:session): session opened for user lunacf(uid=1000) by (uid=0)
Jan 22 16:01:19 tp2 sshd[1523]: Received disconnect from 10.0.2.1 port 65495:11: disconnected by user
```

🌞 Trouver le chemin du fichier de configuration du serveur SSH
```
[lunacf@tp2 ssh]$ cd /etc/ssh
[lunacf@tp2 ssh]$ ls
ssh_config.d/  sshd_config.d/
ssh_config  sshd_config 
```



II. Users

1. Nouveau user


🌞 Créer un nouvel utilisateur
```
[lunacf@tp2 ~]$ sudo useradd -p chocolat -d /home/papier-alu marmotte
```



2. Infos enregistrées par le système


🌞 Prouver que cet utilisateur a été créé
```
[marmotte@tp2 ~]$ cat /etc/passwd | grep marmotte
marmotte:x:1001:1001::/home/papier-alu:/bin/bash
```


🌞 Déterminer le hash du password de l'utilisateur `marmotte`
```
[lunacf@tp2 ~]$ sudo cat /etc/shadow | grep marmotte

marmotte:$6$G9O2j38MXMatz/0L$yjGVBym5wRtNpjp4M6ay4i5VhCornczH9Zpkdl51liHmFlDEfMslA/yMTHTM/lnxWWPRv/L3T6DsuRJ63Gn0A/:19744:0:99999:7:::
```



3. Connexion sur le nouvel utilisateur


🌞 Tapez une commande pour vous déconnecter : fermer votre session utilisateur

```
[lunacf@tp2 ~]$ exit
logout
Connection to 10.0.2.10 closed.
```

🌞 Assurez-vous que vous pouvez vous connecter en tant que l'utilisateur marmotte

```
PS C:\Users\Utilisateur> ssh marmotte@10.0.2.10
marmotte@10.0.2.10's password:
Last login: Tue Jan 23 14:19:37 2024
[marmotte@tp2 ~]$
```

```
[marmotte@tp2 ~]$ ls /home/lunacf
ls: cannot open directory '/home/lunacf': Permission denied
```

```
Partie 2 : Programmes et paquets
```

I. Programmes et processus


1. Run then kill

🌞 Lancer un processus `sleep`
```
[marmotte@tp2 ~]$ ps -u marmotte | grep sleep
    PID TTY          TIME CMD
   1482 pts/0    00:00:00 sleep
```

🌞 Terminez le processus `sleep` depuis le deuxième terminal
```
[marmotte@tp2 ~]$ pkill -x sleep

[marmotte@tp2 ~]$ sleep 1000
Terminated
```


2. Tâche de fond


🌞 Lancer un nouveau processus `sleep`, mais en tâche de fond

```
[marmotte@tp2 ~]$ sleep 1000 &
[1] 1586
```

🌞 Visualisez la commande en tâche de fond

```
[marmotte@tp2 ~]$ jobs | grep sleep
[1]+  Running                 sleep 1000 &

[marmotte@tp2 ~]$ jobs -p
1586
```

3. Find paths


🌞 Trouver le chemin où est stocké le programme `sleep`

```
[marmotte@tp2 ~]$ ls -al /usr/bin | grep sleep
-rwxr-xr-x.  1 root root   36312 Apr 24  2023 sleep
```

🌞 Tant qu'on est à chercher des chemins : trouver les chemins vers tous les fichiers qui s'appellent `.bashrc`

```
[lunacf@tp2 ~]$ sudo find / -name ".bashrc"

/etc/skel/.bashrc
/root/.bashrc
/home/lunacf/.bashrc
/home/marmotte/.bashrc
/home/papier-alu/.bashrc
```


4. La variable PATH

🌞 Vérifier que `sleep`, `bin`, `ping` sont des programmes stockés dans des dossiers de `PATH`

```
[lunacf@tp2 ~]$ echo $PATH
/home/lunacf/.local/bin:/home/lunacf/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

[lunacf@tp2 ~]$ which sleep
/usr/bin/sleep

[lunacf@tp2 ~]$ which ssh
/usr/bin/ssh

[lunacf@tp2 ~]$ which ping
/usr/bin/ping
```


II. Paquets


🌞 Installer le paquet `firefox/git`

```
[lunacf@tp2 ~]$ sudo dnf install firefox
[sudo] password for lunacf:
Last metadata expiration check: 1:28:05 ago on Tue 23 Jan 2024 02:42:17 PM CET.
Dependencies resolved.

Complete!
```

```
[lunacf@tp2 ~]$ sudo dnf install 
[sudo] password for lunacf:
Last metadata expiration check: 1:40:03 ago on Tue 23 Jan 2024 02:42:17 PM CET.
Dependencies resolved.

Complete!
```

🌞 Utiliser une commande pour lancer `git`

```
[lunacf@tp2 ~]$ which git
/usr/bin/git

[lunacf@tp2 ~]$ git show
fatal: not a git repository (or any of the parent directories): .git
```

🌞 Installer le paquet `nginx`

```
[lunacf@tp2 ~]$ sudo dnf install git
[sudo] password for lunacf:
Last metadata expiration check: 1:40:03 ago on Tue 23 Jan 2024 02:42:17 PM CET.
Dependencies resolved.

Installed:
  nginx-1:1.20.1-14.el9_2.1.x86_64                              nginx-core-1:1.20.1-14.el9_2.1.x86_64
  nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                   rocky-logos-httpd-90.14-2.el9.noarch

Complete!
```

🌞 Déterminer


```
[lunacf@tp2 log]$ grep "error_log\|access_log" /etc/nginx/nginx.conf
error_log /var/log/nginx/error.log;
    access_log  /var/log/nginx/access.log  main;
```

```
[lunacf@tp2 ~]$ sudo cat /etc/nginx/nginx.conf
```

🌞 Mais aussi déterminer...

```
pour git

[baseos]
name=Rocky Linux $releasever - BaseOS
mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=BaseOS-$releasever$rltype
#baseurl=http://dl.rockylinux.org/$contentdir/$releasever/BaseOS/$basearch/os/
gpgcheck=1
enabled=1
countme=1
metadata_expire=6h
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Rocky-9
```

```
pour nginx

[appstream]
name=Rocky Linux $releasever - AppStream
mirrorlist=https://mirrors.rockylinux.org/mirrorlist?arch=$basearch&repo=AppStream-$releasever$rltype
#baseurl=http://dl.rockylinux.org/$contentdir/$releasever/AppStream/$basearch/os/
gpgcheck=1
enabled=1
countme=1
metadata_expire=6h
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Rocky-9
```


Partie 3 : Poupée russe

🌞 Récupérer le fichier `meow`

```
PS C:\Users\Utilisateur>  scp C:\Users\Utilisateur\Downloads\meow lunacf@10.0.2.10:/home/lunacf

lunacf@10.0.2.10's password:
meow                                                                                  100%   17MB  81.5MB/s   00:00
```

🌞 Trouver le dossier `dawa/`

```
[lunacf@tp2 ~]$ file /home/lunacf/meow
/home/lunacf/meow: Zip archive data, at least v2.0 to extract
```

```
[lunacf@tp2 ~]$ mv meow meow.zip
[lunacf@tp2 ~]$ ls
meow.zip
```







hash de "..." => echo "..." | md5sum(32 caractères)
-non prédictible ; répétable ; une entrée = même retour; pas d'inverse ; sortie taille fixe ;
entrée infinie -> sortie 16^32

mieux vaut utiliser sha2