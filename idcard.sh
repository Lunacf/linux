#!/bin/bash

name_machine=$(hostnamectl | awk 'NR==1 {print $NF}')

echo "Machine name : ${name_machine}"

source /etc/os-release
name_kernel=$(uname)

echo "OS ${PRETTY_NAME} and kernel version is ${name_kernel} "

IP=$(ip a | grep '2:' -A 2 | grep inet | tr -s ' '| cut -d' ' -f3)
echo "IP : $IP "

ram_av=$(free --mega| tr -s ' '| awk 'NR==2' |cut -d' ' -f7)
ram_tot=$(free --mega| tr -s ' '| awk 'NR==2' |cut -d' ' -f2)
echo "RAM : $ram_av Mo memory available on $ram_tot Mo total memory"

disk=$(df / -H | tr -s ' '| awk 'NR==2' |cut -d' ' -f4)
echo "Disk : $disk space left"

echo "Top 5 processes by RAM usage : "

while read process
do
  name="$(echo $process |tr -s ' ' | cut -d ' ' -f1|awk -F'/' '{print $NF}')"

  echo " - $name"

done <<< "$(ps -eo cmd= --sort=-%mem | head -n5)"


echo "Listening ports : "

while read ports
do
  port="$(echo $ports |cut -d' ' -f5|cut -d':' -f2)"
  protocol="$(echo $ports | tr -s ' '| cut -d' ' -f1)"
  process="$(echo $ports | tr -s ' '| awk -F'/' '{print $NF}' | cut -d'.' -f1) "

  echo " - $port $protocol : $process"

done <<< "$(ss -alputne4H)"



echo "PATH directories :"
echo $PATH | tr ":" "\n"



while read api; do
  image="$(echo $api | cut -d',' -f2 | awk -F'"' '/url/{print $4}')"
  echo "Here is your random cat (jpg file) : $image "
  
done <<< "$(curl -s https://api.thecatapi.com/v1/images/search )"


